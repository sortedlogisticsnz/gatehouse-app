package com.soretdlogistics.gatehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Settings extends AppCompatActivity implements View.OnClickListener {
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String URL = "EndPoint";
    public static final String DEVID = "DevID";
    public static final String DEVKEY = "DevKEY";

    private EditText textEndPoint;
    private EditText textDevID;
    private EditText textDevKey;

    private String EndPoint;
    private String DevID;
    private String DevKEY;
//[
//    {
//        "auth":{
//              "deviceid":"0123456789",
//              "devicekey":"9876543210"
//    },
//        "drivers":{
//              "driver":"1234",
//              "timeout":"2019-08-13 14:56:16" //Time NOW
//    }
//    }
//]
//
//    The app will have to hold three settings:
//
//    Endpoint
//    DeviceID
//    DeviceKey

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Button buttonBack = findViewById(R.id.buttonBack);
        Button buttonSave = findViewById(R.id.buttonSave);
        textEndPoint = findViewById(R.id.TextEndPoint);
        textDevID = findViewById(R.id.TextDevID);
        textDevKey = findViewById(R.id.TextDevKey);



        buttonBack.setOnClickListener(this);
        buttonSave.setOnClickListener(this);

        loadData();
        updateViews();

    }
    private void launchActivity() {

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.buttonBack:
                launchActivity();
            break;
            case R.id.buttonSave:
                saveData();
                launchActivity();
            break;
        }
    }

    public void saveData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(URL, textEndPoint.getText().toString());
        editor.putString(DEVID, textDevID.getText().toString());
        editor.putString(DEVKEY, textDevKey.getText().toString());
        editor.apply();

        Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT);
    }

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EndPoint = sharedPreferences.getString(URL, "");
        DevID = sharedPreferences.getString(DEVID, "");
        DevKEY = sharedPreferences.getString(DEVKEY, "");
    }

    public void updateViews() {
        textEndPoint.setText(EndPoint);
        textDevID.setText(DevID);
        textDevKey.setText(DevKEY);
    }
}
