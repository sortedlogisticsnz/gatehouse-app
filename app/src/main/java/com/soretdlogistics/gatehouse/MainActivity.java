package com.soretdlogistics.gatehouse;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.FormBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {
    public static final String SHARED_PREFS = "sharedPrefs";
    public static final String URL = "EndPoint";
    public static final String DEVID = "DevID";
    public static final String DEVKEY = "DevKEY";


    private String EndPoint;
    private String DevID;
    private String DevKEY;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button1 = findViewById(R.id.button1);
        Button button2 = findViewById(R.id.button2);
        Button button3 = findViewById(R.id.button3);
        Button button4 = findViewById(R.id.button4);
        Button button5 = findViewById(R.id.button5);
        Button button6 = findViewById(R.id.button6);
        Button button7 = findViewById(R.id.button7);
        Button button8 = findViewById(R.id.button8);
        Button button9 = findViewById(R.id.button9);
        Button button0 = findViewById(R.id.button0);
        Button buttonBack = findViewById(R.id.buttonBack);
        Button buttonLogout = findViewById(R.id.buttonLogout);
        ImageView iv2 = findViewById(R.id.imageView4);

        button1.setOnClickListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        button6.setOnClickListener(this);
        button7.setOnClickListener(this);
        button8.setOnClickListener(this);
        button9.setOnClickListener(this);
        button0.setOnClickListener(this);
        buttonBack.setOnClickListener(this);
        buttonLogout.setOnClickListener(this);
        iv2.setOnTouchListener(this);

        loadData();
    }
    private void launchActivity() {

        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        final EditText idtext = findViewById(R.id.TextId);
        String curText = idtext.getText().toString();
        Integer len = curText.length();
        switch (v.getId()){
            case R.id.button1:
                idtext.append("1");
                break;
            case R.id.button2:
                idtext.append("2");
                break;
            case R.id.button3:
                idtext.append("3");
                break;
            case R.id.button4:
                idtext.append("4");
                break;
            case R.id.button5:
                idtext.append("5");
                break;
            case R.id.button6:
                idtext.append("6");
                break;
            case R.id.button7:
                idtext.append("7");
                break;
            case R.id.button8:
                idtext.append("8");
                break;
            case R.id.button9:
                idtext.append("9");
                break;
            case R.id.button0:
                idtext.append("0");
                break;
            case R.id.buttonBack:
                if(!len.equals(0)){
                    idtext.setText(curText.substring(0,len-1));
                }
                break;
            case R.id.buttonLogout:
                OkHttpClient client = new OkHttpClient();
                String url = EndPoint;
                RequestBody formBody = new FormBody.Builder()
                        .add("id", curText)
                        .add("devid", DevID)
                        .add("devkey", DevKEY)
                        .build();
                Request request = new Request.Builder()
                        .url(url)
                        .post(formBody)
                        .build();
                client.newCall(request).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (response.isSuccessful()) {
                            final String myResponse = response.body().string();

                            MainActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    TextView tvr  = findViewById(R.id.textView2);
                                    for (int i=0; i<50; i++) {
                                        tvr.setText(myResponse);
                                        String[] msg = myResponse.split("-");
                                        if (msg[0].matches("ERROR"))
                                        {
                                            tvr.setTextColor(Color.RED);
                                            tvr.setText(msg[1]);
                                        }else if (msg[0].matches("OK"))
                                        {
                                            tvr.setTextColor(Color.GREEN);
                                            tvr.setText(msg[1]);
                                        }else{
                                            tvr.setTextColor(Color.BLUE);
                                            tvr.setText(msg[1]);
                                        }
                                        //tvr.setText(msg[0]);
                                        idtext.setText("");
                                        new CountDownTimer(10000, 1000) {

                                            public void onTick(long millisUntilFinished) {

                                            }

                                            public void onFinish() {
                                                TextView tvr  = findViewById(R.id.textView2);
                                                tvr.setText("");
                                            }
                                        }.start();
                                    }
                                }
                            });
                        }
                    }
                });
              break;
        }
    }



    private int mLastEvent = MotionEvent.ACTION_UP;

    final Runnable runr = new Runnable() {

        @Override
        public void run() {
            launchActivity();
        }
    };

    public void loadData(){
        SharedPreferences sharedPreferences = getSharedPreferences(SHARED_PREFS, MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        EndPoint = sharedPreferences.getString(URL, "");
        DevID = sharedPreferences.getString(DEVID, "");
        DevKEY = sharedPreferences.getString(DEVKEY, "");
    }

    final Handler handel = new Handler();
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        mLastEvent = event.getAction();
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                handel.postDelayed(runr,15000);

                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:

            default:
                handel.removeCallbacks(runr);
                break;

        }
        return true;
    }



}
